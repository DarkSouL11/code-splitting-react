### Code Splitting With React

A boilerplate for code splitting in react with webpack.

### Getting Started

```
git clone https://bitbucket.org/DarkSouL11/code-splitting-react.git    
cd code-splitting-react  
npm i
```

And start writing code in `src` folder.

### How to build

```
`npm run compile` for development build.   
`npm run build` for production build (minified js).  
`npm run watch` for watching the files.  
```

### Included in the build process

```
- Compiles SCSS. (make sure your entry css path is src/css/main.scss)
- Adds vendor prefixes to css.
- Compiles es6 code to es5.
- Creates seperate js named vendor.js for rarely changing compiled node modules code which can be cached by client.
- Injects css links(main.css) and js(vendor and app) scripts to index.html.
- Smart caching of files by generating different filenames for css and js files when content changes. (Only in production build)
- Shortens css class names to reduce size of css file. (Only in production build)
- Minifies js and css. (Only in production build)
```

const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const fs = require('fs-extra');
const incstr = require('incstr');

const createUniqueIdGenerator = () => {
    const index = {};

    const generateNextId = incstr.idGenerator({
        // Removed "d" letter to avoid accidental "ad" construct.
        // @see https://medium.com/@mbrevda/just-make-sure-ad-isnt-being-used-as-a-class-name-prefix-or-you-might-suffer-the-wrath-of-the-558d65502793
        alphabet: 'abcefghijklmnopqrstuvwxyz0123456789'
    });

    return (name) => {
        if (index[name]) {
            return index[name];
        }

        let nextId;

        do {
            // Class name cannot start with a number.
            nextId = generateNextId();
        } while (/^[0-9]/.test(nextId));

        index[name] = generateNextId();

        return index[name];
    };
};

const uniqueIdGenerator = createUniqueIdGenerator();

const generateScopedName = (localName, resourcePath) => {
    const componentName = resourcePath.split('/').slice(-2, -1);
    return uniqueIdGenerator(componentName) + '_' + uniqueIdGenerator(localName);
};

const getModeConfig = prod => {
    return {
        mode: prod ? 'production' : 'development',
        outputFilename: prod ? './js/[name].[chunkHash:6].js' : './js/[name].js',
        cssLoaderConfig: {
            camelCase: true,
            getLocalIdent: (context, localIdentName, localName) => {
                if(prod) return generateScopedName(localName, context.resourcePath);
                return localName;
            },
            importLoaders: 1,
            modules: true,
            minimize: true
        },
        cssOutput: prod ? './css/main.[hash:6].css' : './css/main.css'
    }
}

module.exports = env => {
    // Remove old js/css
    fs.removeSync('./build/css');
    fs.removeSync('./build/js');

    const modeConfig = getModeConfig(env && env.production);

    return {
        mode: modeConfig.mode,
        entry: {
            app: './src/index.js'
        },
        output: {
            path: path.resolve(__dirname + '/build'),
            filename: modeConfig.outputFilename
        },
        module: {
            rules: [
                {
                    test: /\.(jsx|js)$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader'
                },
                {
                    test: /\.scss$/,
                    exclude: /node_modules/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: modeConfig.cssLoaderConfig
                        },
                        'sass-loader',
                        'postcss-loader'
                    ]
                }
            ]
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: modeConfig.cssOutput
            }),
            new HtmlWebpackPlugin({
                template: path.resolve(__dirname, 'src/index.html'),
                filename: 'index.html',
                inject: 'body',
            }),
        ],
        optimization: {
            splitChunks: {
                cacheGroups: {
                    vendors: {
                        name: 'vendors',
                        test: /[\\/]node_modules[\\/]/,
                        chunks: 'all'
                    },
                }
            }
        }
    }
}

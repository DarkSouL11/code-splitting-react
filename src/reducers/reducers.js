export const loading = function(state = 0, action){
    switch (action.type) {
        case 'LOADING_INIT':
            return state + 1;
        case 'LOADING_DONE':
            return state ? state - 1 : 0;
        default:
            return state;
    }
}

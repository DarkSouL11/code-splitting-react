import { applyMiddleware, createStore, combineReducers} from 'redux';
import * as re from './reducers.js';
import thunk from 'redux-thunk';

const reducer = combineReducers({
    loading: re.loading
})

const middleWare = applyMiddleware(thunk)
export const store = createStore(reducer, middleWare);

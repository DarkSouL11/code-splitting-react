import Loadable from 'react-loadable';
import React from 'react';

export const LazyHome = Loadable({
    loading: () => <div> Loading... </div>,
    loader: () => import('./home.jsx')
})

export const LazyAbout = Loadable({
    loading: () => <div> Loading... </div>,
    loader: () => import('./about.jsx')
})

export const LazyTeam = Loadable({
    loading: () => <div> Loading... </div>,
    loader: () => import('./team.jsx')
})

import React from 'react';

// Css for webpack compilation
import c from '../css/main.scss';

export default class Home extends React.Component{
    render(){
        var k = [];
        for(var x = 1; x <= 50; x++){
            k.push('This is home line number: ' + x);
        }

        return (
            <div>
                {k.map((j, i) => {
                    return (
                        <div className={[c.secondaryText, c.bold, c.padding10].join(' ')} key={i}>{j}</div>
                    )
                })}
            </div>
        )
    }
}

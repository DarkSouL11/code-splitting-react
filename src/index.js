import { render } from 'react-dom';
import React from 'react';
import { HashRouter, Route, NavLink, Switch } from 'react-router-dom'
import { store } from './reducers/store';
import { Provider } from 'react-redux';
import { LazyTeam, LazyHome, LazyAbout } from './app/lazyLoadedComponents.jsx';
import f from './utils/funcs.js';

// Css for webpack compilation
import c from './css/main.scss';

class App extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div>
                <div className="nav-bar">
                    <NavLink to="/home" activeClassName={c.uppercase} className={[c.heading, c.padding20].join(' ')}> Home </NavLink>
                    <NavLink to="/about" activeClassName={c.uppercase} className={[c.heading, c.padding20].join(' ')}> About Us </NavLink>
                    <NavLink to="/team" activeClassName={c.uppercase} className={[c.heading, c.padding20].join(' ')}> Our Team </NavLink>
                </div>
                <Switch>
                    <Route path="/home" component={LazyHome}/>
                    <Route path="/about" component={LazyAbout}/>
                    <Route path="/team" component={LazyTeam}/>
                </Switch>
            </div>
        )
    }
}

render(
    <Provider store={store}>
        <HashRouter>
            <App />
        </HashRouter>
    </Provider>,
    document.getElementById('app-container')
)
